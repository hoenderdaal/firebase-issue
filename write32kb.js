
var admin = require("firebase-admin");

var serviceAccount = require("./serviceAccountKey.json");
var data = require("./32kb.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://big-dash-bug-report.firebaseio.com"
});

admin.database().ref(`/test`).remove();
admin.database().ref(`/test`).setWithPriority(data, 1);
