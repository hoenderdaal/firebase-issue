
var admin = require("firebase-admin");

var serviceAccount = require("./serviceAccountKey.json");
var jsonCausingIssue = require("./jsonCausingIssue.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://big-dash-bug-report.firebaseio.com"
});

// 1) Listen on a unused path (overwrite data doesn't)
admin.database().ref(`/test`).on('value', function(snap) {
    console.log('snap', snap.val());
});

